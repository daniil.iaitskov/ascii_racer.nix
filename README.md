Nix derivation for ascii_racer terminal game in pure python.


```
git clone https://gitlab.com/daniil.iaitskov/ascii_racer.nix.git
cd ascii_racer.nix
nix-build 
./result/bin/asciiracer
```

or install by oneliner:
```
nix-env -i -f https://gitlab.com/daniil.iaitskov/ascii_racer.nix/-/archive/master/ascii_racer.nix-master.tar.bz2

```

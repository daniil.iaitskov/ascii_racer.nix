{ pkgs ? import <nixpkgs> {} }:
let
  ascii-racer = pkgs.python39Packages.buildPythonPackage rec {
    pname = "ascii_racer";
    version = "1.0.3";
    name = "${pname}-${version}";

    src = pkgs.fetchFromGitHub {
      owner = "UpGado";
	    repo = "${pname}";
	    rev = "ad8e29ea3cfa8615b0a634ffe8d5b0db5ce7403f";
	    sha256 = "1b9l7n0n20i6s23w45dirqfmll6k1dz46jyn07zkw2fjl37nv5i4";
    };

    doCheck = false;
    buildInputs = with pkgs.python39Packages; [ setuptools pytest ];
    setupPyBuildFlags = [ "-j" "3" ] ;
  };
in ascii-racer
